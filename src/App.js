import React, {Component} from 'react';
import UserCard from './component/UserCard';
import InputUpdate from './component/InputUpdate';
import {Card,Loader} from 'semantic-ui-react';
import faker from 'faker';
import './App.css';

class App extends Component {
    state = {
        card: [],
        updateName: '',
    };

    handleClickAdd = () => {
        console.log('handleClickAdd : ', this.state.card.length + 1);
        this.setState({
            card: [...this.state.card, {
                id: this.state.card.length + 1,
                src: faker.image.avatar(),
                name: faker.name.findName(),
                meta: faker.internet.email(),
                description: faker.lorem.text(),
            }]
        })
    };


    handleClickDelete = (id) => {
        const index = this.state.card.findIndex(element => id === element.id)
        this.setState({
            card: [...this.state.card.slice(0, index), ...this.state.card.slice(index + 1)]
        })
    };

    handleClickUpdate = (id) => {
        const cardUpdate = this.state.card.find(element => id === element.id);
        this.setState({
            updateName: cardUpdate.name,
        })
    };

    componentDidMount() {
        this.setState({
            card: [
                {
                    id: 1,
                    src: faker.image.avatar(),
                    name: faker.name.findName(),
                    meta: faker.internet.email(),
                    description: faker.hacker.phrase(),
                },
                {
                    id: 2,
                    src: faker.image.avatar(),
                    name: faker.name.findName(),
                    meta: faker.internet.email(),
                    description: faker.hacker.phrase(),
                }
            ],
            loading: false,
        })
    }

    render() {
        return (
            <div className="App">
                {this.state.card.length > 0 ?
                <Card.Group itemsPerRow={this.state.card.length}>
                    {this.state.card.map(({id, src, name, meta, description,}) =>
                        <UserCard
                            key={id}
                            src={src}
                            name={name}
                            meta={meta}
                            description={description}
                            onClickAdd={this.handleClickAdd}
                            onClickDelete={() => this.handleClickDelete(id)}
                            onClickUpdate={() => this.handleClickUpdate(id)}
                            updateName={this.state.updateName}
                        />
                    )}
                </Card.Group>
                    :
                    <Loader active inline="centered"/>
                }
            </div>
        );
    }
}

export default App;
