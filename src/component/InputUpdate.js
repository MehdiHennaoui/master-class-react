import React from 'react';
import {Form} from "semantic-ui-react";

const InputUpdate = (props) => {
    return (
        <Form>
            <Form.Field>
                <label>Mise à jour du nom</label>
                <input type="text" value={props.name}/>
            </Form.Field>
        </Form>
    )
}

export default InputUpdate;
