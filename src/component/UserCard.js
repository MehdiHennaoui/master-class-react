import React from 'react';
import {Button, Card, Image} from 'semantic-ui-react';
import InputUpdate from "./InputUpdate";

const UserCard = (props) => {
    return (
        <Card>
            <Image wrapped src={props.src}/>
            <Card.Content>
                <Card.Header>{props.name}</Card.Header>
                <Card.Meta>{props.meta}</Card.Meta>
                <Card.Description>{props.description}</Card.Description>
            </Card.Content>
            <Card.Content extra>
                <div className='ui tree buttons'>
                    <Button onClick={props.onClickAdd} basic color='green'>
                        Ajouter
                    </Button>
                    <Button onClick={props.onClickDelete} basic color="red">
                        Supprimer
                    </Button>
                    <Button onClick={props.onClickUpdate} basic color="red">
                        Mise à jour du nom
                    </Button>
                </div>
            </Card.Content>
            {props.updateName === props.name &&
                <Card.Content extra>
                    <InputUpdate value={props.name}/>
                </Card.Content>
            }
        </Card>
    );
}

export default UserCard;
